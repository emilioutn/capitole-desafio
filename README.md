# Capitole - Desafío

## Introducción

Desarrollar un proyecto Spring Boot que permita manejar los precios por rango de Fechas. 
Este Proyecto se propone como Desafío Capitole.

## Descripción

Se pide generar un sistema Spring Boot, a partir de la siguiente información, proveniente de la Base de Datos de Precios, como desafío Capitole:

```
BRAND_ID      START_DATE                                  END_DATE                         PRICE_LIST                 PRODUCT_ID           PRIORITY                    PRICE           CURR
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

1         2020-06-14-00.00.00                        2020-12-31-23.59.59                        1                        35455                0                        35.50            EUR

1         2020-06-14-15.00.00                        2020-06-14-18.30.00                        2                        35455                1                        25.45            EUR

1         2020-06-15-00.00.00                        2020-06-15-11.00.00                        3                        35455                1                        30.50            EUR

1         2020-06-15-16.00.00                        2020-12-31-23.59.59                        4                        35455                1                        38.95            EUR
```

## Enunciado

En la base de datos de comercio electrónico de la compañía disponemos de la tabla PRICES que refleja el precio final (pvp) y la tarifa que aplica a un producto de una cadena entre unas fechas determinadas. La tabla de ejemplo, se muestra en el apartado anterior.

### Campos: 

```
BRAND_ID: foreign key de la cadena del grupo (1 = ZARA).

START_DATE , END_DATE: rango de fechas en el que aplica el precio tarifa indicado.

PRICE_LIST: Identificador de la tarifa de precios aplicable.

PRODUCT_ID: Identificador código de producto.

PRIORITY: Desambiguador de aplicación de precios. Si dos tarifas coinciden en un rago de fechas se aplica la de mayor prioridad (mayor valor numérico).

PRICE: precio final de venta.

CURR: iso de la moneda.
```

## Desarrollar

Construir una aplicación/servicio en SpringBoot que provea una end point rest de consulta  tal que:

- Acepte como parámetros de entrada: fecha de aplicación, identificador de producto, identificador de cadena.
- Devuelva como datos de salida: identificador de producto, identificador de cadena, tarifa a aplicar, fechas de aplicación y precio final a aplicar.

Se debe utilizar una base de datos en memoria (tipo h2) e inicializar con los datos del ejemplo, (se pueden cambiar el nombre de los campos y añadir otros nuevos si se quiere, elegir el tipo de dato que se considere adecuado para los mismos).

## Prueba de la aplicación

Desarrollar unos test al endpoint rest que validen las siguientes peticiones al servicio con los datos del ejemplo:

- [ ] Test 1: petición a las 10:00 del día 14 del producto 35455 para la brand 1 (ZARA)
- [ ] Test 2: petición a las 16:00 del día 14 del producto 35455 para la brand 1 (ZARA)
- [ ] Test 3: petición a las 21:00 del día 14 del producto 35455 para la brand 1 (ZARA)
- [ ] Test 4: petición a las 10:00 del día 15 del producto 35455 para la brand 1 (ZARA)
- [ ] Test 5: petición a las 21:00 del día 16 del producto 35455 para la brand 1 (ZARA)

Se valorará:

- Diseño y construcción del servicio.
- Calidad de Código.
- Resultados correctos en los test.

### Nota: 
- Solo se han desarrollado pruebas de Sistema, para todos los casos de éxito. 
- Falta desarrollar pruebas de sistema de todos los casos de error, pruebas unitarias y pruebas de Integración.

## Tecnologías utilizadas

Se genera un código Spring Boot con las siguientes tecnologías:

- JDK 17
- Gradle versión 7.1+
- H2
- Swagger
- Lombok
- Mapstruct
- Checkstyle
- Zalando

***

- [ ] Se deja un proyecto escalable preparado para agregarle mayor funcionalidad y con el menor impacto. 
- [ ] Si bien, no se utiliza para este proyecto, Zalando y Mapstruct, es posible utilizarlo al incorporar mayor funcionalidad a la aplicación.

## Prueba desde Swagger o POSTMAN

Si bien el proyecto cuenta con los testcase que se pidieron en el enunciado, es posible hacer pruebas desde Swagger o POSTMAN al endpoint proporcionado.

- Para poder probar desde Swagger:
  - Se debe levantar el proyecto y abrir un explorador al siguiente [localhost](http://localhost:8080/prices/swagger-ui/)
  - Por último, abrir la solapa del Resource de nombre "prices-controller", completar los parámetros de entrada del endpoint y verificar los resultados.

- Para poder probar desde POSTMAN: 
  - Se deja un archivo de exportación en la raíz del proyecto de nombre "Capitole.postman_collection.json", del endpoint con los datos iniciales para poder realizar pruebas.
El mismo debe ser importado a POSTMAN para poder realizar pruebas desde el endpoint generado. 
  - Por último, levantar el proyecto y enviar la petición POST generada desde POSTMAN. Comprobar los resultados.