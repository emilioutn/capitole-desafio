package com.capitole.prices.error;

public class IdentifyAlreadyExistException extends RuntimeException {
    public IdentifyAlreadyExistException(String message) {
        super(message);
    }
}
