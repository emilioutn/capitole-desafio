package com.capitole.prices.repository;

import com.capitole.prices.domain.PriceList;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface PriceListRepositoryImpl extends IPriceListRepository, CrudRepository<PriceList, Long> {
    @Query("select pl " +
            "from PriceList pl " +
            "join fetch pl.product p1 " +
            "join fetch pl.price p2 " +
            "join fetch p1.brand b " +
            "where " +
            "p1.id = ?1 and " +
            "b.id = ?2 and " +
            "?3 between p2.startDate and p2.endDate " +
            "order by p2.priority desc")
    List<PriceList> fetchByProductIdAndBrandIdAndBetweenDatesByMaxPriority(
            Long productId, Long brandId, Date dateCheck);
}
