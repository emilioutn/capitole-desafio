package com.capitole.prices.repository;

import com.capitole.prices.domain.PriceList;

import java.util.Date;
import java.util.List;

public interface IPriceListRepository {
    List<PriceList> fetchByProductIdAndBrandIdAndBetweenDatesByMaxPriority(
            Long productId, Long brandId, Date dateCheck);
}
