package com.capitole.prices.controller;

import com.capitole.prices.dto.FilterProductDto;
import com.capitole.prices.dto.FilterProductResponse;
import com.capitole.prices.service.IProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

@AllArgsConstructor
@RestController
@RequestMapping("/product")
public class PricesController {

    private IProductService productService;

    @GetMapping(value = "/all")
    public ResponseEntity<FilterProductResponse> getProductByFilter(FilterProductDto filterProductDto)
            throws ParseException {
        return ResponseEntity.ok(productService.getProductByFilter(
                        filterProductDto.getProductId(),
                        filterProductDto.getBrandId(),
                        filterProductDto.getPriceDate()
                )
        );
    }
}
