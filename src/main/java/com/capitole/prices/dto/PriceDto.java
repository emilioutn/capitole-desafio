package com.capitole.prices.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PriceDto {
    private Long id;
    private BigDecimal price;
    private String currency;
    private Date startDate;
    private Date endDate;
    private Integer priority;
}
