package com.capitole.prices.dto;

import com.capitole.prices.domain.Price;
import com.capitole.prices.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PriceListDto {
    private Long id;
    private Product product;
    private Price price;
    private LocalDateTime dateCreated;
}
