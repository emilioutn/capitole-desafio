package com.capitole.prices.dto;

import com.capitole.prices.domain.Brand;
import com.capitole.prices.domain.Price;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductDto {
    private Long id;
    private String name;
    private Brand brand;
    private List<Price> prices;
}
