package com.capitole.prices.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FilterProductDto {
    private Long productId;
    private Long brandId;
    private String priceDate;
}
