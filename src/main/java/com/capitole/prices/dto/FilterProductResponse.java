package com.capitole.prices.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class FilterProductResponse {
    private Long productId;
    private Long brandId;
    private Long priceListId;
    private Date startDate;
    private Date endDate;
    private BigDecimal price;
    private String currency;
}
