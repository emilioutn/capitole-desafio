package com.capitole.prices.service;

import com.capitole.prices.dto.FilterProductResponse;

import java.text.ParseException;

public interface IProductService {
    FilterProductResponse getProductByFilter(Long productId, Long brandId, String priceDateStr)
            throws ParseException;
}
