package com.capitole.prices.service;

import com.capitole.prices.domain.PriceList;
import com.capitole.prices.dto.FilterProductResponse;
import com.capitole.prices.repository.IPriceListRepository;
import com.capitole.prices.util.CommonFunctions;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class ProductServiceImpl implements IProductService {

    private CommonFunctions commonFunctions;
    private IPriceListRepository priceListRepository;

    public FilterProductResponse getProductByFilter(Long productId, Long brandId, String priceDateStr)
            throws ParseException {
        FilterProductResponse filterProductResponse = buildFilterProductResponse(
                getPriceListByFilters(productId, brandId, priceDateStr)
        );
        return filterProductResponse;
    }

    private PriceList getPriceListByFilters(Long productId, Long brandId, String priceDateStr)
            throws ParseException {
        Date priceDate = commonFunctions.convertStringToDate(priceDateStr);
        return priceListRepository
                .fetchByProductIdAndBrandIdAndBetweenDatesByMaxPriority(productId, brandId, priceDate).get(0);
    }

    private FilterProductResponse buildFilterProductResponse(PriceList priceSelected) {
        return FilterProductResponse
                .builder()
                .productId(priceSelected.getProduct().getId())
                .brandId(priceSelected.getProduct().getBrand().getId())
                .priceListId(priceSelected.getId())
                .price(priceSelected.getPrice().getPrice())
                .currency(priceSelected.getPrice().getCurrency())
                .startDate(priceSelected.getPrice().getStartDate())
                .endDate(priceSelected.getPrice().getEndDate())
                .build();
    }
}
