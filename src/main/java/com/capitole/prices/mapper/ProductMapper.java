package com.capitole.prices.mapper;

import com.capitole.prices.domain.Product;
import com.capitole.prices.dto.ProductDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductMapper {
    ProductMapper MAPPER = Mappers.getMapper(ProductMapper.class);

    ProductDto mapper(Product entity);

    Product mapper(ProductDto dto);
}
