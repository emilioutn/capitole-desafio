package com.capitole.prices.service;

import com.capitole.prices.dto.FilterProductResponse;
import com.capitole.prices.util.CommonFunctions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class ProductServiceImplTest_SystemTest {

    @Autowired
    private IProductService productService;
    @Autowired
    private CommonFunctions commonFunctions;

    private Long defaultProductId;
    private Long defaultBrandId;
    private String defaultCurrency;

    @BeforeEach
    public void init() {
        defaultProductId = 35455L;
        defaultBrandId = 1L;
        defaultCurrency = "EUR";
    }

    @Test
    public void getProductByFilter_Test1() throws ParseException {
        String priceDate = "2020-06-14 10:00:00";
        FilterProductResponse productByFilter = productService
                .getProductByFilter(defaultProductId, defaultBrandId, priceDate);
        FilterProductResponse filterExpectedResult =
                getFilterProductResult(
                        1L,
                        new BigDecimal("35.50"),
                        "2020-06-14 00:00:00",
                        "2020-12-31 23:59:59"
                );
        assertEquals(productByFilter, filterExpectedResult);
    }

    @Test
    public void getProductByFilter_Test2() throws ParseException {
        String priceDate = "2020-06-14 16:00:00";
        FilterProductResponse productByFilter = productService
                .getProductByFilter(defaultProductId, defaultBrandId, priceDate);
        FilterProductResponse filterExpectedResult =
                getFilterProductResult(
                        2L,
                        new BigDecimal("25.45"),
                        "2020-06-14 15:00:00",
                        "2020-06-14 18:30:00"
                );
        assertEquals(productByFilter, filterExpectedResult);
    }

    @Test
    public void getProductByFilter_Test3() throws ParseException {
        String priceDate = "2020-06-14 21:00:00";
        FilterProductResponse productByFilter = productService
                .getProductByFilter(defaultProductId, defaultBrandId, priceDate);
        FilterProductResponse filterExpectedResult =
                getFilterProductResult(
                        1L,
                        new BigDecimal("35.50"),
                        "2020-06-14 00:00:00",
                        "2020-12-31 23:59:59"
                );
        assertEquals(productByFilter, filterExpectedResult);
    }

    @Test
    public void getProductByFilter_Test4() throws ParseException {
        String priceDate = "2020-06-15 10:00:00";
        FilterProductResponse productByFilter = productService
                .getProductByFilter(defaultProductId, defaultBrandId, priceDate);
        FilterProductResponse filterExpectedResult =
                getFilterProductResult(
                        3L,
                        new BigDecimal("30.50"),
                        "2020-06-15 00:00:00",
                        "2020-06-15 11:00:00"
                );
        assertEquals(productByFilter, filterExpectedResult);
    }

    @Test
    public void getProductByFilter_Test5() throws ParseException {
        String priceDate = "2020-06-16 21:00:00";
        FilterProductResponse productByFilter = productService
                .getProductByFilter(defaultProductId, defaultBrandId, priceDate);
        FilterProductResponse filterExpectedResult =
                getFilterProductResult(
                        4L,
                        new BigDecimal("38.95"),
                        "2020-06-15 16:00:00",
                        "2020-12-31 23:59:59"
                );
        assertEquals(productByFilter, filterExpectedResult);
    }

    // TODO - ALL METHOD OF ERROR

    private FilterProductResponse getFilterProductResult(Long priceListId, BigDecimal price, String startDateStr,
                                                         String endDateStr) throws ParseException {
        Date startDate = commonFunctions.convertStringToDate(startDateStr);
        Date endDate = commonFunctions.convertStringToDate(endDateStr);
        Timestamp startDateTs = new Timestamp(startDate.getTime());
        Timestamp endDateTs = new Timestamp(endDate.getTime());
        return FilterProductResponse
                .builder()
                .productId(defaultProductId)
                .brandId(defaultBrandId)
                .priceListId(priceListId)
                .price(price)
                .startDate(startDateTs)
                .endDate(endDateTs)
                .currency(defaultCurrency)
                .build();
    }
}
